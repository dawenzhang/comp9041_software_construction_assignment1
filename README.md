# COMP9041 Software Construction Assignment 1 Semester 2, 2018

## a Perl implemented git lite


### hashed or ordered commit id

This project uses hashed commit id while it provides an ordered commit id by default which is required by the assignment specification. The hashed commit can be used by an immediate ``` return $num; ``` after line 1526 in ``` sub atm_num2hex ``` and ``` return $hashed; ``` after line 1506 in ``` sub atm_hex2num ```

### incremental commit

not supported

### use

add _legit_ to the environment or copy _legit_ to the targeting directory and run directly as
```
./legit.pl
```

### available commands

required argument: ```$argument```

optional argument: ```[argument]```

e.g.

```
./legit.pl commit [-a] -m $message
```

-a is optional

```
./legit.pl commit -a -m "this is a required argument"
```

- init

```
./legit.pl init
```

- add


```
./legit.pl add $filenames
```

- commit

```
./legit.pl commit [-a] -m $message
```

- log

```
./legit.pl log
```

- show

```
./legit.pl show $commit-id:$filename
```

- rm

```
./legit.pl rm [--force] [--cached] $filenames
```

- status

```
./legit.pl status
```

- branch

```
./legit.pl branch [-d] [branch-name]
```

- checkout

```
./legit.pl checkout $branch-name
```

- merge

```
./legit.pl merge $(branch-name|commit) -m $message
```

### __if you find this repo, please do not use any code fragment from it for academic assessment__