#!/usr/bin/perl -w

# Author:        David Zhang
# Created:       18:19, 18 Sep 2018
# Last Modified: 02:45,  2 Oct 2018
#
# Program runs as a legit for version control
# Usage: legit.pl <command> [<args>]
# Commands:
# init       Create an empty legit repository
# add        Add file contents to the index
# commit     Record changes to the repository
# log        Show commit log
# show       Show file at particular state
# rm         Remove files from the current directory and from the index
# status     Show the status of files in the current directory, index, and repository
# branch     list, create or delete a branch
# checkout   Switch branches or restore current directory files
# merge      Join two development histories together

####################################################################################################
# libraries used
####################################################################################################

use Digest::MD5 qw(md5_hex);
use Algorithm::Merge qw(merge diff3 traverse_sequences3);

####################################################################################################
# pre-defined global variables
####################################################################################################

$converter_file = "./.legit/converter";
$legit_path = "./.legit";
$legit_git_path = "./.legit/.git";
$legit_git_index = "./.legit/.git/index";
$legit_git_logs_path ="./.legit/.git/logs";
$legit_git_logs_head ="./.legit/.git/logs/HEAD";
$legit_git_logs_refs_path ="./.legit/.git/logs/refs";
$legit_git_logs_refs_heads_path ="./.legit/.git/logs/refs/heads";
$legit_git_logs_head_master = "./.legit/.git/logs/refs/heads/master";
$legit_git_refs_path ="./.legit/.git/refs";
$legit_git_refs_heads_path ="./.legit/.git/refs/heads";
$legit_git_head = "./.legit/.git/HEAD";
$legit_git_head_master = "./.legit/.git/refs/heads/master";
$legit_git_objects_path = "./.legit/.git/objects";

####################################################################################################
# subroutines to handle the command-line arguments
####################################################################################################

sub args_check {
    if(@_ eq 0) {
        return 2;
    }else {
        if($_[0] eq "init") {
            return handle_init(@_);
        }elsif($_[0] eq "add") {
            return handle_add(@_);
        }elsif($_[0] eq "commit") {
            return handle_commit(@_);
        }elsif($_[0] eq "log") {
            return handle_gitlog(@_);
        }elsif($_[0] eq "show") {
            return handle_show(@_);
        }elsif($_[0] eq "rm") {
            return handle_rm(@_);
        }elsif($_[0] eq "status") {
            return handle_status(@_);
        }elsif($_[0] eq "branch") {
            return handle_branch(@_);
        }elsif($_[0] eq "checkout") {
            return handle_checkout(@_);
        }elsif($_[0] eq "merge") {
            return handle_merge(@_);
        }
        return (3, $_[0]);
    }
}

sub handle_command {
    @arg_test_result = args_check(@ARGV);
    if($arg_test_result[0] eq 2) {
        print 'Usage: legit.pl <command> [<args>]

These are the legit commands:
   init       Create an empty legit repository
   add        Add file contents to the index
   commit     Record changes to the repository
   log        Show commit log
   show       Show file at particular state
   rm         Remove files from the current directory and from the index
   status     Show the status of files in the current directory, index, and repository
   branch     list, create or delete a branch
   checkout   Switch branches or restore current directory files
   merge      Join two development histories together', "\n\n";
        exit 1;
    }if($arg_test_result[0] eq 3) {
        my $cmd = $arg_test_result[1];
        print STDERR "legit.pl: error: unknown command $cmd\n";
        print 'Usage: legit.pl <command> [<args>]

These are the legit commands:
   init       Create an empty legit repository
   add        Add file contents to the index
   commit     Record changes to the repository
   log        Show commit log
   show       Show file at particular state
   rm         Remove files from the current directory and from the index
   status     Show the status of files in the current directory, index, and repository
   branch     list, create or delete a branch
   checkout   Switch branches or restore current directory files
   merge      Join two development histories together', "\n\n";
        exit 1;
    }else {
        exit $arg_test_result[0];
    }
}

sub handle_init {
    if(!(-f $converter_file)) {
        if(open my $cvt, '>', $converter_file) {
            close $cvt;
        }
    }
    my @init_result = init(@ARGV);
    if($init_result[0] eq 1) {
        print STDERR "legit.pl: error: .legit already exists\n";
    } elsif ($init_result[0] eq 2) {
        print STDERR "usage: legit.pl init\n";
    } else {
        print "Initialized empty legit repository in .legit\n";
        return 0;
    }
    return 1;
}

sub handle_add {
    my @add_result = add(@ARGV);
    if($add_result[0] eq 1) {
        print STDERR "legit.pl: error: no .legit directory containing legit repository exists\n";
    }elsif($add_result[0] eq 2) {
        print STDERR "legit.pl: error: internal error Nothing specified, nothing added.\nMaybe you wanted to say 'git add .'?\n";
    }elsif($add_result[0] eq 3) {
        print STDERR "legit.pl: error: can not open '", $add_result[1], "'\n";
    }elsif($add_result[0] eq 0) {
        return 0;
    }
    return 1;
}

sub handle_commit {
    my @commit_result = commit(@ARGV);
    if($commit_result[0] eq 1) {
        print STDERR "legit.pl: error: no .legit directory containing legit repository exists\n";
    }elsif($commit_result[0] eq 2) {
        print STDERR "nothing to commit\n";
    }elsif($commit_result[0] eq 3) {
        print STDERR "usage: legit.pl commit [-a] -m commit-message\n";
    }elsif($commit_result[0] eq 0) {
        my $last = -1;
        if(open my $converter_in, '<', $converter_file) {
            while(my $line = <$converter_in>) {
                if($line =~ /^(.+?)\s(?:.+?)\s*$/) {
                    $last = $1;
                }
            }
            close $converter_in;
        }
        $last += 1;
        my $cn = $commit_result[1];
        if(open my $converter_out, '>>', $converter_file) {
            print $converter_out $last." ".$cn."\n";
            close $converter_out;
        }
        print "Committed as commit $last\n";
        return 0;
    }
    return 1;
}

sub handle_gitlog {
    my @log_result = gitlog(@ARGV);
    if($log_result[0] eq 1) {
        print STDERR "legit.pl: error: no .legit directory containing legit repository exists\n";
    }elsif($log_result[0] eq 2) {
        print STDERR "legit.pl: error: your repository does not have any commits yet\n";
    }elsif($log_result[0] eq 3) {
        print STDERR "usage: legit.pl log\n";
    }elsif($log_result[0] eq 0) {
        my ($code, @logs) = @log_result;
        for(my $log_count = @logs - 1; $log_count >= 0; $log_count -= 1) {
            if($logs[$log_count] =~ /.+?\s(.+?)\scommit(?: \(initial\))?: (.*)\s(?:.+?)\s*$/) {
                print atm_hex2num($1), " ", $2, "\n";
            }
        }
        return 0;
    }
    return 1;
}

sub handle_show {
    my @show_result;
    my $arg_show = $ARGV[0];
    if(@ARGV eq 2) {
        if($ARGV[1] =~ /^(.*):(.*)$/) {
            if($1 eq "") {
                @show_result = show(@ARGV);
            }else {
                my $arg_sec = atm_num2hex($1).':'.$2;
                @show_result = show($arg_show, $arg_sec);
            }
        }else {
            @show_result = show(@ARGV);
        }
    }else {
        @show_result = show(@ARGV);
    }
    if($show_result[0] eq 1) {
        print STDERR "legit.pl: error: no .legit directory containing legit repository exists\n";
    }elsif($show_result[0] eq 2) {
        print STDERR "legit.pl: error: your repository does not have any commits yet\n";
    }elsif($show_result[0] eq 3) {
        print STDERR "usage: legit.pl show <commit>:<filename>\n";
    }elsif($show_result[0] eq 4) {
        my $cmt = atm_hex2num($show_result[1]);
        print STDERR "legit.pl: error: unknown commit '$cmt'\n";
    }elsif($show_result[0] eq 5) {
        my $cmt = atm_hex2num($show_result[1]);
        my $fn = $show_result[2];
        print STDERR "legit.pl: error: '$fn' not found in commit $cmt\n";
    }elsif($show_result[0] eq 6) {
        my $fn = $show_result[1];
        print STDERR "legit.pl: error: '$fn' not found in index\n";
    }elsif($show_result[0] eq 7) {
        my $fn = $show_result[1];
        print STDERR "legit.pl: error: invalid filename '$fn'\n";
    }elsif($show_result[0] eq 0) {
        my $fc = $show_result[1];
        print $fc;
        return 0;
    }
    return 1;
}

sub handle_rm {
    my @rm_result = rm(@ARGV);
    if($rm_result[0] eq 1) {
        print STDERR "legit.pl: error: no .legit directory containing legit repository exists\n";
    }elsif($rm_result[0] eq 2) {
        print STDERR "legit.pl: error: your repository does not have any commits yet\n";
    }elsif($rm_result[0] eq 3) {
        print STDERR "error: internal error usage: git rm [<options>] [--] <file>...\n\n-n, --dry-run         dry run\n-q, --quiet           do not list removed files\n--cached              only remove from the index\n-f, --force           override the up-to-date check\n-r                    allow recursive removal\n--ignore-unmatch      exit with a zero status even if nothing matched\n";
    }elsif($rm_result[0] eq 5) {
        my $fn = $rm_result[1];
        print STDERR "legit.pl: error: '$fn' has changes staged in the index\n";
    }elsif($rm_result[0] eq 4) {
        my $fn = $rm_result[1];
        print STDERR "legit.pl: error: '$fn' in repository is different to working file\n";
    }elsif($rm_result[0] eq 6) {
        my $fn = $rm_result[1];
        print STDERR "legit.pl: error: '$fn' in index is different to both working file and repository\n";
    }elsif($rm_result[0] eq 7) {
        my $fn = $rm_result[1];
        print STDERR "legit.pl: error: '$fn' is not in the legit repository\n";
    }elsif($rm_result[0] eq 0) {
        return 0;
    }
    return 1;
}

sub handle_status {
    my @status_result = status(@ARGV);
    if($status_result[0] eq 1) {
        print STDERR "legit.pl: error: no .legit directory containing legit repository exists\n";
    }elsif($status_result[0] eq 2) {
        print STDERR "legit.pl: error: your repository does not have any commits yet\n";
    }elsif($status_result[0] eq 0) {
        my $status_output = $status_result[1];
        print $status_output;
        return 0;
    }
    return 1;
}

sub handle_branch {
    my @branch_result = branch(@ARGV);
    if($branch_result[0] eq 1) {
        print STDERR "legit.pl: error: no .legit directory containing legit repository exists\n";
    }elsif($branch_result[0] eq 2) {
        print STDERR "legit.pl: error: your repository does not have any commits yet\n";
    }elsif($branch_result[0] eq 3) {
        print STDERR "usage: legit.pl branch [-d] <branch>\n";
    }elsif($branch_result[0] eq 4) {
        my $brn = $branch_result[1];
        print STDERR "legit.pl: error: invalid branch name '$brn'\n";
    }elsif($branch_result[0] eq 5) {
        my $brn = $branch_result[1];
        print STDERR "legit.pl: error: branch '$brn' already exists\n";
    }elsif($branch_result[0] eq 6) {
        my $brn = $branch_result[1];
        print STDERR "legit.pl: error: can not delete branch '$brn'\n";
    }elsif($branch_result[0] eq 7) {
        my $brn = $branch_result[1];
        print STDERR "legit.pl: error: branch '$brn' does not exist\n";
    }elsif($branch_result[0] eq 8) {
        my $brn = $branch_result[1];
        print STDERR "legit.pl: error: branch '$brn' has unmerged changes\n";
    }elsif($branch_result[0] eq 0) {
        if($branch_result[1] eq 0) {
            ($_, $_, @branches) = @branch_result;
            foreach my $b(@branches) {
                print $b, "\n";
            }
        }elsif($branch_result[1] eq 1) {

        }elsif($branch_result[1] eq 2) {
            my $brn = $branch_result[2];
            print "Deleted branch '$brn'\n";
        }
        return 0;
    }
    return 1;
}

sub handle_checkout {
    my @checkout_result = checkout(@ARGV);
    if($checkout_result[0] eq 1) {
        print STDERR "legit.pl: error: no .legit directory containing legit repository exists\n";
    }elsif($checkout_result[0] eq 2) {
        print STDERR "legit.pl: error: your repository does not have any commits yet\n";
    }elsif($checkout_result[0] eq 3) {
        my $brn = $checkout_result[1];
        print STDERR "legit.pl: error: unknown branch '$brn'\n";
    }elsif($checkout_result[0] eq 4) {
        my $brn = $checkout_result[1];
        print STDERR "Already on '$brn'\n";
    }elsif($checkout_result[0] eq 5) {
        my ($code, @overwritten) = @checkout_result;
        print STDERR "legit.pl: error: Your changes to the following files would be overwritten by checkout:\n";
        foreach my $over(sort @overwritten) {
            print STDERR $over, "\n";
        }
    }elsif($checkout_result[0] eq 0) {
        my $brn = $checkout_result[1];
        print "Switched to branch '$brn'\n";
        return 0;
    }
    return 1;
}

sub handle_merge {
    my @merge_result = merge_branch(@ARGV);
    if($merge_result[0] eq 1) {
        print STDERR "legit.pl: error: no .legit directory containing legit repository exists\n";
    }elsif($merge_result[0] eq 2) {
        print STDERR "legit.pl: error: your repository does not have any commits yet\n";
    }elsif($merge_result[0] eq 3) {
        print STDERR "usage: legit.pl merge <branch|commit> -m message\n";
    }elsif($merge_result[0] eq 4) {
        print STDERR "legit.pl: error: empty commit message\n";
    }elsif($merge_result[0] eq 5) {
        my $brn = $merge_result[1];
        print STDERR "legit.pl: error: unknown branch '$brn'\n";
    }elsif($merge_result[0] eq 6) {
        print STDERR "Already up to date\n";
    }elsif($merge_result[0] eq 0) {
        if($merge_result[1] eq 1) {
            print "Fast-forward: no commit created\n";
        }elsif($merge_result[1] eq 2) {
            my ($code0, $code1, @conflicting_files) = @merge_result;
            print "legit.pl: error: These files can not be merged:\n";
            foreach my $conflict_file(@conflicting_files) {
                print $conflict_file, "\n";
            }
        }elsif($merge_result[1] eq 0) {
            my ($code0, $code1, $commit_hex, @auto_merging_files) = @merge_result;
            foreach my $auto_merged_file(@auto_merging_files) {
                print "Auto-merging ", $auto_merged_file, "\n";
            }
            my $last = -1;
            if(open my $converter_in, '<', $converter_file) {
                while(my $line = <$converter_in>) {
                    if($line =~ /^(.+?)\s(?:.+?)\s*$/) {
                        $last = $1;
                    }
                }
                close $converter_in;
            }
            $last += 1;
            my $cn = $commit_hex;
            if(open my $converter_out, '>>', $converter_file) {
                print $converter_out $last." ".$cn."\n";
                close $converter_out;
            }
            print "Committed as commit $last\n";
        }
        return 0;
    }
    return 1;
}

####################################################################################################
# subroutines responding to the command calls
####################################################################################################

# subroutine for init command
# return 0 if no error, 1 if directory already exists, 2 if arguments invalid
sub init {
    # check if only one argument
    if(@ARGV > 1) {
        return 2;
    }
    # check if the .legit directory already exists
    if(-d $legit_path) {
        return 1;
    }else {
        # create the .legeit and .legit/.git directory
        mkdir($legit_path);
        mkdir($legit_git_path);
        return 0;
    }
}

# subroutine for add command
# return 0 if no error, 1 if no initialized repository, 2 if no adding files, 3 with the file name if a file is not a valid file
sub add {
    my ($command, @files) = @_;
    if(!(-d $legit_path)) {
        return 1;
    }
    if(@files eq 0) {
        return 2;
    }
    # read the current index
    my %index_files = ();
    my @index_withdraw_result = util_import_index_with_dict();
    if($index_withdraw_result[0] eq 0) {
        %index_files = @index_withdraw_result[1 .. $#index_withdraw_result];
    }
    # if the file does not exist but in the index then remove from index, otherwise report file not exist
    foreach my $file(@files) {
        if(!(-f $file) and $index_files{$file} and !($index_files{$file} =~ /^\s*.+?\s*Deleted/)) {
            rm("rm", "--force", "--cached", $file);
            return 0;
        }elsif(!(-f $file) or !($file =~ /^[a-zA-Z0-9][a-zA-Z0-9.-_]*$/)) {
            return (3, $file);
        }
    }
    # add all the files from command-line arguments to the index
    foreach my $file(@files) {
        my @withdraw_result = atm_withdraw_file($file);
        if($withdraw_result[0] eq 0) {
            my @archive_result = util_archive_object("blob", @withdraw_result[1 .. $#withdraw_result]);
            if($archive_result[0] eq 0) {
                my $file_hex = $archive_result[1];
                # if already added before then update index, else add new entry
                if($index_files{$file}) {
                    if( $index_files{$file} =~ /^(.{32})\s(?:.+?)(?:(?:\s(.*))?)\s*$/) {
                        $index_files{$file} = $1." ".$file_hex." ".$2;
                    }
                }else {
                    $index_files{$file} = $file_hex." ".$file_hex." "."00000000000000000000000000000000";
                }
            }
        }
    }
    # export to the index
    my $export_index_result = util_export_index_with_dict(%index_files);
    if($export_index_result eq 0) {
        return 0;
    }else {
        return 1;
    }
}

# subroutine for commit command
# return 0 if no error, 1 if no initialized repository, 2 if no added files, 3 if arguments invalid
sub commit {
    if(!(-d $legit_path) or !(-d $legit_git_path)) {
        return 1;
    }
    # check whether the -a argument is given
    if(@_ eq 4 and $_[1] eq "-a" and $_[2] eq "-m" and $_[3] ne "") {
        my @index_import_result = util_import_index_with_array();
        if($index_import_result[0] eq 0) {
            foreach my $index_line(@index_import_result[1 .. $#index_import_result]) {
                my $index_line_trim = $index_line;
                $index_line_trim =~ s/^\s+|\s+$//g;
                if($index_line_trim =~ /^(.+?)\s(?:.{32})\s(?:.+?)/) {
                    if(-f $1) {
                        add("add", $1);
                    }else {
                        rm("rm", "--force", "--cached", $1);
                    }
                }
            }
        }
    }elsif(!(@_ eq 3 and $_[1] eq "-m" and $_[2] ne "")) {
        return 3;
    }
    # check the current branch and if no branch has been made then make a master branch
    my $head = "";
    my $legit_git_head_current = "";
    my @head_withdraw_result = atm_withdraw_sized_file($legit_git_head);
    if($head_withdraw_result[0] eq 0) {
        $legit_git_head_current = $head_withdraw_result[1];
    }else {
        my @head_store_result = atm_store_file($legit_git_head, $legit_git_head_master);
        if($head_store_result[0] eq 0) {
            $legit_git_head_current = $legit_git_head_master;
        }
    }
    # load the current commit as head; if none then load 0 as head
    my @current_head_withdraw_result = atm_withdraw_sized_file($legit_git_head_current);
    if($current_head_withdraw_result[0] eq 0) {
        $head = $current_head_withdraw_result[1];
        $head =~ s/^\s+|\s+$//g;
    }else {
        if(!(-d $legit_git_refs_path)) {
            mkdir($legit_git_refs_path);
        }
        if(!(-d $legit_git_refs_heads_path)) {
            mkdir($legit_git_refs_heads_path);
        }
        $head = "00000000000000000000000000000000";
    }
    # check if any file has been changed since last commit
    my %index_files = ();
    my %index_added_files = ();
    my $changed = 0;
    my @index_import_dict_result = util_import_index_with_dict();
    if($index_import_dict_result[0] eq 0) {
        %index_files = @index_import_dict_result[1 .. $#index_import_dict_result];
        foreach my $index_entry(keys (%index_files)) {
            if($index_files{$index_entry} =~ /^(.{32})\s(.+?)(?:(?:\s(.*))?)$/) {
                if($2 ne $3) {
                    $changed = 1;
                }
                $index_added_files{$index_entry} = $2;
            }
        }
    }
    if($changed eq 0) {
        return 2;
    }
    # make a tree object to refer to the blobs
    my @tree_content = ();
    foreach my $pending_file(sort(keys %index_added_files)) {
        push @tree_content, $pending_file." ".$index_added_files{$pending_file}."\n";
    }
    my @tree_archive_result = util_archive_object("tree", @tree_content);
    my $tree_hex = "";
    if($tree_archive_result[0] eq 0) {
        $tree_hex = $tree_archive_result[1];
    }
    # make a commit object to refer to the tree
    my $commit_message = $_[-1];
    my $commit_content = $head." ".$tree_hex." ".$commit_message." ".time."\n";
    my @commit_archive_result = util_archive_object("commit", $commit_content);
    my $commit_hex = "";
    if($commit_archive_result[0] eq 0) {
        $commit_hex = $commit_archive_result[1];
    }
    # modify the current branch head to point to the commit object
    $head = $commit_hex;
    $head =~ s/^\s+|\s+$//g;
    atm_store_file($legit_git_head_current, $head);
    # update the index
    foreach my $index_update_file(keys(%index_files)) {
        if($index_files{$index_update_file} =~ /^(.{32})\s([^\s]{32})((?:\s.*)?)\s*$/) {
            $index_files{$index_update_file} = $1." ".$2." ".$2;
        }elsif($index_files{$index_update_file} =~ /^(.+?)\sDeleted\s00000000000000000000000000000000\s*$/) {
            
        }elsif($index_files{$index_update_file} =~ /^(.+?)\sDeleted.*?\s*$/) {
            $index_files{$index_update_file} = $1." "."Deleted"." "."Deleted";
        }
    }
    util_export_index_with_dict(%index_files);
    # update the log
    if(!(-d $legit_git_logs_path)) {
        mkdir($legit_git_logs_path);
    }
    if(-f $legit_git_logs_head and -s $legit_git_logs_head and open my $git_logs_head_in, '<', $legit_git_logs_head) {
        $logs_head_current = <$git_logs_head_in>;
        close $git_logs_head_in;
    }else {
        if(!(-d $legit_git_logs_refs_path)) {
            mkdir($legit_git_logs_refs_path);
        }
        if(!(-d $legit_git_logs_refs_heads_path)) {
            mkdir($legit_git_logs_refs_heads_path);
        }
        if(atm_store_file($legit_git_logs_head, $legit_git_logs_head_master) eq 0) {
            $logs_head_current = $legit_git_logs_head_master;
        }
    }
    my $current_message = "";
    if(-s $logs_head_current) {
        $current_message = "commit: ".$commit_message;
    }else {
        $current_message = "commit (initial): ".$commit_message;
    }
    atm_append_file($logs_head_current, $head." ".$commit_hex." ".$current_message." ".time."\n");
    return (0, $head);
}

# subroutine for log command
# return 0 if no error, 1 if directory already exists, 2 if no commits exist, 3 if arguments invalid
sub gitlog {
    # check if repository initialized
    if(!(-d $legit_path)) {
        return 1;
    }
    # check if has commit log
    if(!(-f $legit_git_logs_head and -s $legit_git_logs_head)) {
        return 2;
    }
    # check if arguments valid
    if(@_ ne 1) {
        return 3;
    }
    # check the log location
    my @commit_logs = ();
    my $log_loc = "";
    my @log_head_withdraw_result = atm_withdraw_file($legit_git_logs_head);
    if($log_head_withdraw_result[0] eq 0) {
        $log_loc = $log_head_withdraw_result[1];
    }
    # read the log
    my @log_withdraw_result = atm_withdraw_file($log_loc);
    if($log_withdraw_result[0] eq 0) {
        @commit_logs = @log_withdraw_result[1 .. $#log_withdraw_result];
    }
    return (0, @commit_logs);
}

# subroutine for show command
# return 0 if no error, 1 if directory already exists, 2 if no commits exist, 3 if arguments invalid, 4 if unknown commit, 5 if file not found in specified commit. 6 if file not found in index if no commit specified, 7 if file name invalid
sub show {
    # check if repository initialized
    if(!(-d $legit_path)) {
        return 1;
    }
    # check if has commit log
    if(!(-f $legit_git_logs_head and -s $legit_git_logs_head)) {
        return 2;
    }
    if($_[1] =~ /^(.*):(.*)$/) {
        my $commit_hex = $1;
        my $file = $2;
        my $file_blob = "";
        if($file =~ /[^a-zA-Z0-9.,-_]+/) {
            return (7, $file);
        }
        # if the commit argument before : is blank then read from the index
        if($commit_hex eq "") {
            my @index_import_result = util_import_index_with_array();
            if($index_import_result[0] eq 0) {
                my $found_in_index = 0;
                foreach my $file_record(@index_import_result[1 .. $#index_import_result]) {
                    if($file_record =~ /^(.+?)\s(?:.{32})\s(.+?)\s.*/) {
                        $fb = $2;
                        if($1 eq $file and $fb =~ /^.{32}$/) {
                            $file_blob = $fb;
                            $found_in_index = 1;
                        }
                    }
                }
                if($found_in_index eq 0) {
                    return (6, $file);
                }
            }else {
                return 2;
            }
        }else {
            # read the commit object
            my @commit_fetch_result = util_fetch_object($commit_hex);
            if($commit_fetch_result[0] eq 0) {
                my $commit_file_line = $commit_fetch_result[1];
                if($commit_file_line =~ /^(?:.+?)\s(.+?)\s.*$/) {
                    # read the tree object
                    my $tree_hex = $1;
                    my @tree_fetch_result = util_fetch_object($tree_hex);
                    if($tree_fetch_result[0] eq 0) {
                        my $found = 0;
                        my $tree_file_line = $tree_fetch_result[1];
                        foreach my $tree_line(@tree_fetch_result[1 .. $#tree_fetch_result]) {
                            if($tree_line =~ /^$file\s(.+?)\s*$/) {
                                $file_blob = $1;
                                $found = 1;
                            }
                        }
                        if($found eq 0) {
                            return (5, $commit_hex, $file);
                        }
                    }else {
                        return (4, $commit_hex);
                    }
                }else {
                    return (4, $commit_hex);
                }
            }else {
                return (4, $commit_hex);
            }
        }
        # read the blob
        my @blob_fetch_result = util_fetch_object($file_blob);
        if($blob_fetch_result[0] eq 0) {
            my $file_content = "";
            foreach my $archived_line(@blob_fetch_result[1 .. $#blob_fetch_result]) {
                $file_content = $file_content.$archived_line;
            }
            return (0, $file_content);
        }else {
            return (5, $commit_hex, $file);
        }
    }else {
        return 3;
    }
}

# subroutine for rm command
# return 0 if no error, 1 if directory already exists, 2 if no commits exist, 3 if arguments invalid, 4 if file not added, 5 if not committed, 6 if file is pending. 7 if file not tracked in the repository
sub rm {
    if(!(-d $legit_path)) {
        return 1;
    }
    # check if has commit log
    if(!(-f $legit_git_logs_head and -s $legit_git_logs_head)) {
        return 2;
    }
    # check if arguments valid
    if(@_ eq 1) {
        return 3;
    }
    my @filenames = ();
    my $is_force = 0;
    my $is_cached = 0;
    if(($_[1] eq "--force" and $_[2] eq "--cached") or ($_[1] eq "--cached" and $_[2] eq "--force")) {
        $is_force = 1;
        $is_cached = 1;
        @filenames = @_[3 .. $#_];
    }elsif($_[1] eq "--force") {
        $is_force = 1;
        @filenames = @_[2 .. $#_];
    }elsif($_[1] eq "--cached") {
        $is_cached = 1;
        @filenames = @_[2 .. $#_];
    }else {
        @filenames = @_[1 .. $#_];
    }
    # read the index
    my @index_import_dict_result = util_import_index_with_dict();
    if($index_import_dict_result[0] eq 0) {
        my %found = ();
        my %index_files = @index_import_dict_result[1 .. $#index_import_dict_result];
        foreach my $index_entry(keys %index_files) {
            foreach my $fn(@filenames) {
                my $is_staged = 0;
                my $is_changed = 0;
                my $is_deleted = 0;
                # check the status of the files so that to show warning for dangerous deletion
                if($index_files{$index_entry} =~ /^(.{32})\s(.+?)(?:(?:\s(.*))?)\s*$/) {
                    if($fn eq $index_entry) {
                        $found{$fn} = 1;
                        if($2 eq "Deleted") {
                            return (7, $fn);
                        }
                        if($2 ne $3) {
                            $is_staged = 1;
                        }
                        my $staging_file = $2;
                        if($staging_file =~ /^.{32}$/) {
                            my @archived_file_withdraw_result = atm_withdraw_file($fn);
                            if($archived_file_withdraw_result[0] eq 0) {
                                my $file_hex = atm_compute_object_hex("blob", @archived_file_withdraw_result[1 .. $#archived_file_withdraw_result]);
                                if($staging_file ne $file_hex) {
                                    $is_changed = 1;
                                }
                            }else {
                                $is_deleted = 1;
                            }
                        }
                        if($is_force eq 0 and $is_cached eq 0 and $is_deleted eq 0) {
                            if($is_staged eq 1 and $is_changed eq 1) {
                                return (6, $fn);
                            }elsif($is_staged eq 0 and $is_changed eq 1) {
                                return (4, $fn);
                            }elsif($is_staged eq 1 and $is_changed eq 0) {
                                return (5, $fn);
                            }
                        }elsif($is_force eq 0 and $is_cached eq 1) {
                            if($is_staged eq 1 and $is_changed eq 1) {
                                return (6, $fn);
                            }
                        }
                    }
                }else {
                    delete $index_files{$index_entry};
                }
            }
        }
        # if any file not found then report warning
        foreach my $file2find(@filenames) {
            if(!($found{$file2find})) {
                return (7, $file2find);
            }
        }
        # deletion
        foreach my $file2delete(@filenames) {
            if($is_cached eq 0) {
                unlink $file2delete;
            }
            if($index_files{$file2delete} =~ /^(.{32})\s(.+?)(?:(?:\s(.*))?)\s*$/) {
                if($3 eq "00000000000000000000000000000000" or $3 eq "") {
                    delete $index_files{$file2delete};
                }else {
                    $index_files{$file2delete} = $1." "."Deleted"." ".$3."\n";
                }
            }
        }
        # update index
        util_export_index_with_dict(%index_files);
        return 0;
    }else {
        return 2;
    }
}

# subroutine for status command
# return 0 if no error, 1 if directory already exists, 2 if no commits exist
sub status {
    # check if repository initialized
    if(!(-d $legit_path)) {
        return 1;
    }
    # check if has commit log
    if(!(-f $legit_git_logs_head and -s $legit_git_logs_head)) {
        return 2;
    }
    # collect file info for determining status
    my %all_file_records = ();
    my %directory_files = ();
    my %index_files = ();
    foreach my $directory_file(<'*'>) {
        if(-f $directory_file) {
            $all_file_records{$directory_file} = 1;
            $directory_files{$directory_file} = 1;
            my @file_withdraw_result = atm_withdraw_file($directory_file);
            if($file_withdraw_result[0] eq 0) {
                my @file_content = @file_withdraw_result[1 .. $#file_withdraw_result];
                $directory_files{$directory_file} = atm_compute_object_hex("blob", @file_content);
            }
        }
    }
    my @index_import_dict_result = util_import_index_with_dict();
    if($index_import_dict_result[0] eq 0) {
        %index_files = @index_import_dict_result[1 .. $#index_import_dict_result];
        foreach my $index_entry(keys %index_files) {
            $all_file_records{$index_entry} = 1;
        }
    }
    # specify status for each file
    my $status_output = "";
    foreach my $file_to_check(sort keys(%all_file_records)) {
        if(!($directory_files{$file_to_check}) and $index_files{$file_to_check} =~ /^(?:.+?)\s.{32}\s(.{32})?\s*$/ and $1 and $1 ne "00000000000000000000000000000000") {
            $all_file_records{$file_to_check} = "file deleted";
        }elsif(!($directory_files{$file_to_check}) and $index_files{$file_to_check} =~ /^(?:.+?)\sDeleted\s(?:.{32})\s*$/) {
            $all_file_records{$file_to_check} = "deleted";
        }elsif(($directory_files{$file_to_check}) and (!($index_files{$file_to_check}) or $index_files{$file_to_check} =~ /^(?:.+?)\sDeleted\s?(?:.{32})?\s*$/)) {
            $all_file_records{$file_to_check} = "untracked";
        }elsif(!($directory_files{$file_to_check}) and $index_files{$file_to_check} =~ /^(?:.+?)\s.{32}\s(.{32})?\s*$/ and (!$1 or $1 eq "00000000000000000000000000000000")) {
            $all_file_records{$file_to_check} = "added to index";
        }elsif(($directory_files{$file_to_check}) and ($index_files{$file_to_check}) and ($index_files{$file_to_check} =~ /^(?:.+?)\s.{32}\s?(?:.{32})?\s*$/)) {
            my $current = $directory_files{$file_to_check};
            if($index_files{$file_to_check} =~ /^(?:.+?)\s$current\s$current\s*$/) {
                $all_file_records{$file_to_check} = "same as repo";
            }elsif($index_files{$file_to_check} =~ /^$current\s$current\s?(?:.{32})?\s*$/) {
                $all_file_records{$file_to_check} = "added to index";
            }elsif($index_files{$file_to_check} =~ /^(?:.+?)\s$current\s(?:.{32})\s*$/) {
                $all_file_records{$file_to_check} = "file changed, changes staged for commit";
            }elsif($index_files{$file_to_check} =~ /^(?:.+?)\s(.{32})\s(.{32})\s*$/) {
                if($1 eq $2) {
                    $all_file_records{$file_to_check} = "file changed, changes not staged for commit";
                }else {
                    $all_file_records{$file_to_check} = "file changed, different changes staged for commit";
                }
            }
        }
        if($all_file_records{$file_to_check} ne 1) {
            $status_output = $status_output.$file_to_check.' - '.$all_file_records{$file_to_check}."\n";
        }
    }
    return (0, $status_output);
}

# subroutine for branch command
# return 0 if no error, 1 if directory already exists, 2 if no commits exist, 3 if arguments invalid, 4 if branch name invalid, 5 if branch already exists when creating, 6 if branch not exists when deleting, 7 if branch change unmerged when deleting
sub branch {
    # check if repository initialized
    if(!(-d $legit_path)) {
        return 1;
    }
    # check if has commit log
    if(!(-f $legit_git_logs_head and -s $legit_git_logs_head)) {
        return 2;
    }
    # show all branch
    if(@_ eq 1) {
        my @branches = ();
        foreach my $b(sort(<"$legit_git_refs_heads_path/*">)) {
            if($b =~ /\/([^\/]+?)\s*$/) {
                push @branches, $1;
            }
        }
        return (0, 0, @branches);
    }
    # make new branch
    if(@_ eq 2) {
        # check if branch name legal
        if($_[1] =~ /^[a-zA-Z0-9][a-zA-Z0-9-_]*$/) {
            if(-f $legit_git_refs_heads_path.'/'.$_[1]) {
                return (5, $_[1]);
            }
            # point the new branch head to current commit
            my $current_commit = "";
            my $legit_git_head_current = "";
            my @head_withdraw_result = atm_withdraw_sized_file($legit_git_head);
            if($head_withdraw_result[0] eq 0) {
                $legit_git_head_current = $head_withdraw_result[1];
                my @current_head_withdraw_result = atm_withdraw_sized_file($legit_git_head_current);
                if($current_head_withdraw_result[0] eq 0) {
                    $current_commit = $current_head_withdraw_result[1];
                }
            }
            atm_store_file($legit_git_refs_heads_path.'/'.$_[1], $current_commit);
            # initialize log file
            if($legit_git_head_current =~ /\/([^\/]+?)\s*$/) {
                my @git_target_logs_withdraw_result = atm_withdraw_file($legit_git_logs_refs_heads_path.'/'.$1);
                if($git_target_logs_withdraw_result[0] eq 0) {
                    atm_store_file($legit_git_logs_refs_heads_path.'/'.$_[1], @git_target_logs_withdraw_result[1 .. $#git_target_logs_withdraw_result]);
                }
            }
            return (0, 1);
        }else {
            return (4, $_[1]);
        }
    }
    # branch deletion
    if(@_ eq 3) {
        my $br_name = "";
        if($_[1] eq "-d") {
            $br_name = $_[2];
        }elsif($_[2] eq "-d") {
            $br_name = $_[1];
        }else {
            return 3;
        }
        # check if branch name legal
        if($br_name =~ /^[a-zA-Z0-9][a-zA-Z0-9-_]*$/) {
            my $legit_git_head_current = "";
            my @head_withdraw_result = atm_withdraw_sized_file($legit_git_head);
            if($head_withdraw_result[0] eq 0) {
                $legit_git_head_current = $head_withdraw_result[1];
            }
            my $branch2delete = $br_name;
            # check if is current branch
            if($legit_git_head_current =~ /$legit_git_refs_heads_path\/$branch2delete/) {
                return (6, $br_name);
            }
            # check if branch exists
            if(!(-f $legit_git_refs_heads_path.'/'.$branch2delete)) {
                return (7, $br_name);
            }
            my $current_commit_hex = "";
            my $deleting_branch_hex = "";
            my @current_head_withdraw_result = atm_withdraw_file($legit_git_head_current);
            my @deleting_branch_withdraw_result = atm_withdraw_file($legit_git_refs_heads_path.'/'.$branch2delete);
            if($current_head_withdraw_result[0] eq 0) {
                $current_commit_hex = $current_head_withdraw_result[1];
            }
            if($deleting_branch_withdraw_result[0] eq 0) {
                $deleting_branch_hex = $deleting_branch_withdraw_result[1];
            }else {
                return (7, $br_name);
            }
            # check if have unmerged commit
            my $unmerged = 1;
            foreach my $a_log_file(<"$legit_git_logs_refs_heads_path/*">) {
                if($a_log_file ne $legit_git_logs_refs_heads_path.'/'.$br_name) {
                    my @a_logs_result = atm_withdraw_file($a_log_file);
                    if($a_logs_result[0] eq 0) {
                        foreach my $log_l_t(@a_logs_result[1 .. $#a_logs_result]) {
                            if($log_l_t =~ /^.+?\s(.+?)\s.+$/) {
                                if($1 eq $deleting_branch_hex) {
                                    $unmerged = 0;
                                }
                            }
                        }
                    }
                }
            }
            if($unmerged eq 1) {
                return (8, $br_name);
            }

            unlink $legit_git_refs_heads_path.'/'.$branch2delete;
            unlink $legit_git_logs_refs_heads_path.'/'.$branch2delete;
            return (0, 2, $branch2delete);
        }else {
            return (4, $br_name);
        }
    } 
    return 3;
}

# subroutine for checkout command
# return 0 if no error, 1 if directory already exists, 2 if no commits exist, 3 if branch unknown, 4 if is current branch, 5 if overwriting
sub checkout {
    # check if repository initialized
    if(!(-d $legit_path)) {
        return 1;
    }
    # check if has commit log
    if(!(-f $legit_git_logs_head and -s $legit_git_logs_head)) {
        return 2;
    }
    # check if branch name is legal and exists
    if($_[1] =~ /^[a-zA-Z0-9][a-zA-Z0-9-_]*$/) {
        if(!(-f $legit_git_refs_heads_path.'/'.$_[1])) {
            return (3, $_[1]);
        }else {
            my $legit_git_head_current = "";
            my @head_withdraw_result = atm_withdraw_sized_file($legit_git_head);
            if($head_withdraw_result[0] eq 0) {
                $legit_git_head_current = $head_withdraw_result[1];
            }
            # check if is current branch
            if($legit_git_head_current eq $legit_git_refs_heads_path.'/'.$_[1]) {
                return (4, $_[1]);
            }
        }
        # the following steps check files that 1) have difference between current working copy and committed copy with difference between latest added copy and committed copy so that cannot checkout because of overwriting; 2) only exist in latest commit so that need to be copied; 3) only exist in working copy and same with latest commit so that need to be deleted
        my $target_branch_commit = "";
        my $target_branch_tree = "";
        my %target_files = ();
        my %check_files = ();
        # read index
        my @index_import_dict_result = util_import_index_with_dict();
        if($index_import_dict_result[0] eq 0) {
            %target_files = @index_import_dict_result[1 .. $#index_import_dict_result];
            %check_files = @index_import_dict_result[1 .. $#index_import_dict_result];
        }
        # read target branch head
        my @head_withdraw_result = atm_withdraw_file($legit_git_refs_heads_path.'/'.$_[1]);
        if($head_withdraw_result[0] eq 0) {
            $target_branch_commit = $head_withdraw_result[1];
        }
        # read target branch commit
        my @commit_object_fetch_result = util_fetch_object($target_branch_commit);
        if($commit_object_fetch_result[0] eq 0) {
            $target_branch_tree = $commit_object_fetch_result[1];
        }
        # read target branch tree
        if($target_branch_tree =~ /^.{32}\s(.{32})/) {
            my @target_branch_tree_fetch_result = util_fetch_object($1);
            if($target_branch_tree_fetch_result[0] eq 0) {
                my @overwritten = ();
                foreach my $target_branch_file(@target_branch_tree_fetch_result[1 .. $#target_branch_tree_fetch_result]) {
                    if($target_branch_file =~ /^(.+?)\s(.+?)\s*$/) {
                        my $committed_file = $1;
                        my $committed_blob = $2;
                        # file exists but not exists in target branch commit
                        if(!($target_files{$committed_file}) and (-f $committed_file)) {
                            push @overwritten, $committed_file;
                        }
                        if($target_files{$committed_file} and $target_files{$committed_file} =~ /^(.+?)\s(.+?)\s(.+?)\s*$/) {
                            # compare file with committed blob
                            my @checking_file_content = ();
                            my @checking_file_withdraw_result = atm_withdraw_file($committed_file);
                            if($checking_file_withdraw_result[0] eq 0) {
                                @checking_file_content = @checking_file_withdraw_result[1 .. $#checking_file_withdraw_result];
                            }
                            my $current_hex = atm_compute_object_hex("blob", $committed_file);
                            # check if current working copy not equal to added copy and added copy not equal to committed copy
                            if($2 ne $3 and $current_hex ne $2) {
                                if($3 ne $committed_blob) {
                                    push @overwritten, $committed_file;
                                }
                            }
                        }
                    }
                }
                # check if overwriting occurs
                if(@overwritten > 0) {
                    return (5, @overwritten);
                }
            }
            # if everything going well then checkout
            foreach my $target_branch_file(@target_branch_tree_fetch_result[1 .. $#target_branch_tree_fetch_result]) {
                if($target_branch_file =~ /^(.+?)\s(.+?)\s*$/) {
                    my $committed_file = $1;
                    my $committed_blob = $2;
                    if($committed_blob ne "Deleted") {
                        $check_files{$committed_file} = 0;
                    }
                    # check if the file exists or not in the current working copies
                    if(!($target_files{$committed_file})) {
                        # copy the file which is currently not exist from the committed blob
                        $target_files{$committed_file} = $committed_blob." ".$committed_blob." ".$committed_blob;
                        if($committed_blob =~ /^\s*(.{32})\s*$/) {
                            my @committed_blob_fetch_result = util_fetch_object($1);
                            if($committed_blob_fetch_result[0] eq 0) {
                                atm_store_file($committed_file, @committed_blob_fetch_result[1 .. $#committed_blob_fetch_result]);
                            }
                        }
                    }else {
                        # replace the file by blob and update the index
                        if($target_files{$committed_file} =~ /^(.+?)\s(.+?)\s(.+?)\s*$/) {
                            my $first_added = $1;
                            my @checking_file_content = ();
                            my @checking_file_withdraw_result = atm_withdraw_file($committed_file);
                            if($checking_file_withdraw_result[0] eq 0) {
                                @checking_file_content = @checking_file_withdraw_result[1 .. $#checking_file_withdraw_result];
                            }
                            my $current_hex = atm_compute_object_hex("blob", @checking_file_content);
                            if(@checking_file_content eq 0) {
                                $current_hex = "Deleted";
                            }
                            if($2 ne $3 or $current_hex ne $2) {
                                if($3 eq $committed_blob) {
                                    $target_files{$committed_file} = $1." ".$2." ".$committed_blob;
                                }
                            }else {
                                if($committed_blob =~ /^\s*(.{32})\s*$/) {
                                    my @committed_blob_fetch_result = util_fetch_object($1);
                                    if($committed_blob_fetch_result[0] eq 0) {
                                        atm_store_file($committed_file, @committed_blob_fetch_result[1 .. $#committed_blob_fetch_result]);
                                    }
                                }
                                $target_files{$committed_file} = $first_added." ".$committed_blob." ".$committed_blob;
                            }
                        }elsif($target_files{$committed_file} =~ /^(.+?)\s(.+?)\s*$/) {
                            $target_files{$committed_file} = $1." ".$2." ".$committed_blob;
                        }
                    }
                }
            }
        }
        # delete those archived currently existing file which are not in the target branch commit
        foreach my $checking_file(keys %check_files) {
            if($check_files{$checking_file} ne 0 and $check_files{$checking_file} =~ /^(.+?)\s(.+?)\s(.+?)\s*$/) {
                my @checking_file_content = ();
                my @checking_file_withdraw_result = atm_withdraw_file($checking_file);
                if($checking_file_withdraw_result[0] eq 0) {
                    @checking_file_content = @checking_file_withdraw_result[1 .. $#checking_file_withdraw_result];
                }
                my $current_hex = atm_compute_object_hex("blob", @checking_file_content);
                if($2 eq $3 and $current_hex eq $2) {
                    delete $target_files{$checking_file};
                    unlink $checking_file;
                }
            }
        }
        # update index and heads
        util_export_index_with_dict(%target_files);
        atm_store_file($legit_git_head, $legit_git_refs_heads_path.'/'.$_[1]);
        atm_store_file($legit_git_logs_head, $legit_git_logs_refs_heads_path.'/'.$_[1]);
        return (0, $_[1]);
    }else {
        return (3, $_[1]);
    }
}

# subroutine for merge command
# return 0 if no error, 1 if directory already exists, 2 if no commits exist, 3 if arguments invalid, 4 if commit message empty, 5 if unknown branch, 6 if already up-to-date
sub merge_branch {
    my $branch2merge = "";
    my $merge_message = "";
    # check if arguments legal
    if(@_ < 4) {
        return 4;
    }
    if($_[1] eq "-m") {
        $merge_message = $_[2];
        $branch2merge = $_[3];
    }elsif($_[2] eq "-m") {
        $branch2merge = $_[1];
        $merge_message = $_[3];
    }else {
        return 3;
    }
    # check if branch exists
    if(!(-f $legit_git_refs_heads_path.'/'.$branch2merge)) {
        return (5, $branch2merge);
    }
    # read heads, commits and logs
    my $legit_git_head_current = "";
    my @head_withdraw_result = atm_withdraw_sized_file($legit_git_head);
    if($head_withdraw_result[0] eq 0) {
        $legit_git_head_current = $head_withdraw_result[1];
    }
    my $current_branch_name = "";
    if($legit_git_head_current =~ /\/([^\/]+?)\s*$/) {
        $current_branch_name = $1;
    }
    my $current_branch_commit = "";
    my @current_head_withdraw_result = atm_withdraw_sized_file($legit_git_head_current);
    if($current_head_withdraw_result[0] eq 0) {
        $current_branch_commit = $current_head_withdraw_result[1];
    }
    my $target_branch_commit = "";
    my @branch2merge_head_withdraw_result = atm_withdraw_sized_file($legit_git_refs_heads_path.'/'.$branch2merge);
    if($branch2merge_head_withdraw_result[0] eq 0) {
        $target_branch_commit = $branch2merge_head_withdraw_result[1];
    }
    # check if already up-to-date
    if($current_branch_commit eq $target_branch_commit) {
        return 6;
    }
    my @current_branch_logs = ();
    my @current_branch_logs_withdraw_result = atm_withdraw_sized_file($legit_git_logs_refs_heads_path.'/'.$current_branch_name);
    if($current_branch_logs_withdraw_result[0] eq 0) {
        @current_branch_logs = @current_branch_logs_withdraw_result[1 .. $#current_branch_logs_withdraw_result];
    }
    my @target_branch_logs = ();
    my @target_branch_logs_withdraw_result = atm_withdraw_sized_file($legit_git_logs_refs_heads_path.'/'.$branch2merge);
    if($target_branch_logs_withdraw_result[0] eq 0) {
        @target_branch_logs = @target_branch_logs_withdraw_result[1 .. $#target_branch_logs_withdraw_result];
    }
    # find the common ancestor by logs
    my $common_ancestor = "";
    my $ancestor_index = 0;
    foreach my $log_l_c(@current_branch_logs) {
        if($log_l_c =~ /^.+?\s(.+?)\s.*$/) {
            my $scanning_commit = $1;
            my $scanning_index = 0;
            foreach my $log_l_t(@target_branch_logs) {
                if($log_l_t =~ /^.+?\s(.+?)\s.*$/) {
                    if($scanning_commit eq $1) {
                        $common_ancestor = $scanning_commit;
                        $ancestor_index = $scanning_index;
                    }
                }
                $scanning_index += 1;
            }
        }
    }
    # if the target branch commit is the common ancestor, then already up-to-date
    if($target_branch_commit eq $common_ancestor) {
        return 6;
    }
    # if the current branch commit is the common ancestor, namely the branching node of the target branch, then fast forward
    if($current_branch_commit eq $common_ancestor) {
        checkout("checkout", $branch2merge);
        atm_store_file($legit_git_logs_refs_heads_path.'/'.$current_branch_name, @target_branch_logs);
        atm_store_file($legit_git_logs_head, $legit_git_logs_refs_heads_path.'/'.$current_branch_name);
        atm_store_file($legit_git_head_current, $target_branch_commit);
        atm_store_file($legit_git_head, $legit_git_head_current);
        return (0, 1);
    }
    # read the blobs of these three commits
    my %current_commit_blobs = ();
    my %target_commit_blobs = ();
    my %ancestor_commit_blobs = ();
    if($current_branch_commit =~ /^\s*(.{32})\s*$/) {
        my @current_tree_object_fetch_result = util_fetch_object($1);
        if($current_tree_object_fetch_result[0] eq 0) {
            my $commit_line = $current_tree_object_fetch_result[1];
            if($commit_line =~ /^\s*.{32}\s(.{32})\s.+\s*$/) {
                my @current_tree_fetch_result = util_fetch_tree_with_dict($1);
                if($current_tree_fetch_result[0] eq 0) {
                    %current_commit_blobs = @current_tree_fetch_result[1 .. $#current_tree_fetch_result];
                }
            }
        }
    }
    if($target_branch_commit =~ /^\s*(.{32})\s*$/) {
        my @target_tree_object_fetch_result = util_fetch_object($1);
        if($target_tree_object_fetch_result[0] eq 0) {
            my $commit_line = $target_tree_object_fetch_result[1];
            if($commit_line =~ /^\s*.{32}\s(.{32})\s.+\s*$/) {
                my @target_tree_fetch_result = util_fetch_tree_with_dict($1);
                if($target_tree_fetch_result[0] eq 0) {
                    %target_commit_blobs = @target_tree_fetch_result[1 .. $#target_tree_fetch_result];
                }
            }
        }
    }
    if($common_ancestor =~ /^\s*(.{32})\s*$/) {
        my @ancestor_tree_object_fetch_result = util_fetch_object($1);
        if($ancestor_tree_object_fetch_result[0] eq 0) {
            my $commit_line = $ancestor_tree_object_fetch_result[1];
            if($commit_line =~ /^\s*.{32}\s(.{32})\s.+\s*$/) {
                my @ancestor_tree_fetch_result = util_fetch_tree_with_dict($1);
                if($ancestor_tree_fetch_result[0] eq 0) {
                    %ancestor_commit_blobs = @ancestor_tree_fetch_result[1 .. $#ancestor_tree_fetch_result];
                }
            }
        }
    }
    # merge files
    my %destination_blobs = ();
    my @merged_blobs = ();
    my @conflict = ();
    foreach my $filename(sort (keys %target_commit_blobs)) {
        # merge files that have two versions the same blob
        if(((!$ancestor_commit_blobs{$filename}) and !($target_commit_blobs{$filename})) or (($ancestor_commit_blobs{$filename} and $target_commit_blobs{$filename}) and ($ancestor_commit_blobs{$filename} eq $target_commit_blobs{$filename})) or ((!$ancestor_commit_blobs{$filename}) and ($target_commit_blobs{$filename} eq "Deleted")) or ((!$target_commit_blobs{$filename}) and ($ancestor_commit_blobs{$filename} eq "Deleted"))) {
            $destination_blobs{$filename} = $current_commit_blobs{$filename};
        }elsif(((!$ancestor_commit_blobs{$filename}) and !($current_commit_blobs{$filename})) or (($ancestor_commit_blobs{$filename} and $current_commit_blobs{$filename}) and ($ancestor_commit_blobs{$filename} eq $current_commit_blobs{$filename})) or ((!$ancestor_commit_blobs{$filename}) and ($current_commit_blobs{$filename} eq "Deleted")) or ((!$current_commit_blobs{$filename}) and ($ancestor_commit_blobs{$filename} eq "Deleted"))) {
            $destination_blobs{$filename} = $target_commit_blobs{$filename};
        }elsif(($target_commit_blobs{$filename} and $current_commit_blobs{$filename}) and ($target_commit_blobs{$filename} eq $current_commit_blobs{$filename})) {
            $destination_blobs{$filename} = $current_commit_blobs{$filename};
        }else {
            # fetch blobs and do three-way merge
            my @ancestor_blob = ();
            my @current_blob = ();
            my @target_blob = ();
            if($ancestor_commit_blobs{$filename} and $ancestor_commit_blobs{$filename} =~ /^\s*(.{32})\s*$/) {
                my @ancestor_blobs_fetch_result = util_fetch_object($1);
                if($ancestor_blobs_fetch_result[0] eq 0) {
                    @ancestor_blob = @ancestor_blobs_fetch_result[1 .. $#ancestor_blobs_fetch_result];
                }
            }
            if($current_commit_blobs{$filename} and $current_commit_blobs{$filename} =~ /^\s*(.{32})\s*$/) {
                my @current_blobs_fetch_result = util_fetch_object($1);
                if($current_blobs_fetch_result[0] eq 0) {
                    @current_blob = @current_blobs_fetch_result[1 .. $#current_blobs_fetch_result];
                }
            }
            if($target_commit_blobs{$filename} and $target_commit_blobs{$filename} =~ /^\s*(.{32})\s*$/) {
                my @target_blobs_fetch_result = util_fetch_object($1);
                if($target_blobs_fetch_result[0] eq 0) {
                    @target_blob = @target_blobs_fetch_result[1 .. $#target_blobs_fetch_result];
                }
            }
            my $conflicted = 0;
            my @merged = merge(\@ancestor_blob, \@current_blob, \@target_blob, { 
                CONFLICT => sub {
                    if($conflicted eq 0) {
                        push @conflict, $filename;
                        $conflicted = 1;
                    }
                }
            });
            # archive the blob
            if(@merged > 0) {
                my @archive_merged_result = util_archive_object("blob", @merged);
                if($archive_merged_result[0] eq 0) {
                    my $file_hex = $archive_merged_result[1];
                    if($file_hex =~ /^(.{2})(.+)$/) {
                        my $file_directory = $1;
                        my $file_name = $2;
                        $destination_blobs{$filename} = $file_hex;
                        push @merged_blobs, $filename;
                    }
                }
            }
        }
    }
    # if conflicts occur then return with conflicting list
    if(@conflict > 0) {
        return (0, 2, @conflict);
    }
    # if any file does not exist with destination blob then replace it with latest committed blob
    foreach my $current_filename(sort (keys %current_commit_blobs)) {
        if(!($destination_blobs{$current_filename})) {
            $destination_blobs{$current_filename} = $current_commit_blobs{$current_filename};
        }
    }
    # make the tree object
    my $head = $current_branch_commit;
    my $tree_content = "";
    foreach my $pending_file(sort(keys %destination_blobs)) {
        $tree_content = $tree_content.$pending_file." ".$destination_blobs{$pending_file}."\n";
    }
    my $tree_hex = "";
    my @tree_archive_result = util_archive_object("tree", $tree_content);
    if($tree_archive_result[0] eq 0) {
        $tree_hex = $tree_archive_result[1];
    }
    # make the commit object
    my $commit_message = $merge_message;
    my $commit_content = "";
    $commit_content = $commit_content.$head." ".$tree_hex." ".$commit_message." ".time."\n";
    my $commit_hex = "";
    my @commit_archive_result = util_archive_object("commit", $commit_content);
    if($commit_archive_result[0] eq 0) {
        $commit_hex = $commit_archive_result[1];
    }
    # update the heads
    $head = $commit_hex;
    $head =~ s/^\s+|\s+$//g;
    atm_store_file($legit_git_head_current, $head);
    atm_store_file($legit_git_refs_heads_path.'/'.$branch2merge, $head);
    # update index
    my %index_update_files = ();
    my @index_import_result = util_import_index_with_dict();
    if($index_import_result[0] eq 0) {
        my %index_read = @index_import_result[1 .. $#index_import_result];
        foreach $index_file_title(keys (%index_read)) {
            if($index_read{$index_file_title} =~ /^(.{32})\s([^\s]{32})(?:(?:\s+?(.+?))?)\s*$/) {
                if($destination_blobs{$index_file_title}) {
                    my @working_file_withdraw_result = atm_withdraw_file($index_file_title);
                    if($working_file_withdraw_result[0] eq 0) {
                        my $file_hex = atm_compute_object_hex("blob", @working_file_withdraw_result[1 .. $#working_file_withdraw_result]);
                        if($3 and $2 eq $file_hex and $2 eq $3) {
                            $index_update_files{$index_file_title} = $1." ".$destination_blobs{$index_file_title}." ".$destination_blobs{$index_file_title};
                            my @blob_file_fetch_result = util_fetch_object($destination_blobs{$index_file_title});
                            if($blob_file_fetch_result[0] eq 0) {
                                atm_store_file($index_file_title, @blob_file_fetch_result[1 .. $#blob_file_fetch_result]);
                            }
                        }
                    }else {
                        $index_update_files{$index_file_title} = $1." ".$2." ".$destination_blobs{$index_file_title};
                    }
                }else {
                    $index_update_files{$index_file_title} = $1." ".$2." ".$2;
                }
            }elsif($index_read{$index_file_title} =~ /^(.+?)\sDeleted\s00000000000000000000000000000000\s*$/) {
                
            }elsif($index_read{$index_file_title} =~ /^(.+?)\s(Deleted.*?)\s*$/) {
                if($destination_blobs{$index_file_title}) {
                    $index_update_files{$index_file_title} = $1." "."Deleted"." ".$destination_blobs{$index_file_title};
                }else {
                    $index_update_files{$index_file_title} = $1." "."Deleted"." ".$2;
                }
            }
        }
        foreach my $destination(keys(%destination_blobs)) {
            if(!($index_update_files{$destination})) {
                $index_update_files{$destination} = $destination_blobs{$destination}." ".$destination_blobs{$destination}." ".$destination_blobs{$destination};
                if($destination_blobs{$destination} =~ /^(.{2})(.{30})$/) {
                    atm_copy_file($legit_git_objects_path.'/'.$1.'/'.$2, $destination);
                }
            }
        }
    }
    util_export_index_with_dict(%index_update_files);
    # update logs
    if(!(-d $legit_git_logs_path)) {
        mkdir($legit_git_logs_path);
    }
    my @logs_head_withdraw_result = atm_withdraw_sized_file($legit_git_logs_head);
    if($logs_head_withdraw_result[0] eq 0) {
        $logs_head_current = $logs_head_withdraw_result[1];
    }else {
        if(!(-d $legit_git_logs_refs_path)) {
            mkdir($legit_git_logs_refs_path);
        }
        if(!(-d $legit_git_logs_refs_heads_path)) {
            mkdir($legit_git_logs_refs_heads_path);
        }
        if(atm_store_file($legit_git_logs_head, $legit_git_logs_head_master) eq 0) {
            $logs_head_current = $legit_git_logs_head_master;
        }
    }
    my $current_message = "";
    if(-s $logs_head_current) {
        $current_message = "commit: ".$commit_message;
    }else {
        $current_message = "commit (initial): ".$commit_message;
    }
    my @combined_logs = (@current_branch_logs, @target_branch_logs[($ancestor_index + 1) .. $#target_branch_logs]);
    # sort logs
    my @sorted_combined_logs = sort{
    $ta = 0;
    $tb = 0;
    if($a =~ /^.+?\s(.+?)\s.+\s*$/) {
        $ta = atm_hex2num($1);
    }
    if($b =~ /^.+?\s(.+?)\s.+\s*$/) {
        $tb = atm_hex2num($1);
    }
    $ta <=> $tb;
    } @combined_logs;
    push @sorted_combined_logs, $head." ".$commit_hex." ".$current_message." ".time."\n";
    atm_store_file($logs_head_current, @sorted_combined_logs);
    atm_append_file($legit_git_logs_refs_heads_path.'/'.$branch2merge, $target_branch_commit." ".$commit_hex." ".$current_message." ".time."\n");
    return (0, 0, $commit_hex, @merged_blobs);
}

####################################################################################################
# atomic operation subroutines
####################################################################################################

# implement file copy as some systems may have not installed Copy module
sub atm_copy_file {
    my $file_origin = $_[0];
    my $file_target = $_[1];
    my @withdraw_result = atm_withdraw_file($file_origin);
    if($withdraw_result[0] eq 0) {
        my $store_result = atm_store_file($file_target, @withdraw_result[1 .. $#withdraw_result]);
        if($store_result eq 0) {
            return 0;
        }else {
            return 1;
        }
    }else {
        return 1;
    }
}

# convert commit hex to commit number
sub atm_hex2num {
    my $hashed = $_[0];
    if(open my $converter_in, '<', $converter_file) {
        my $found = 0;
        while(my $line = <$converter_in>) {
            if($line =~ /^(.+?)\s$hashed\s*$/) {
                close $converter_in;
                return $1;
            }
        }
        if($found eq 0) {
            return $hashed;
        }
        close $converter_in;
    }else {
        return $hashed;
    }
}

# convert commit number to commit hex
sub atm_num2hex {
    my $num = $_[0];
    if(open my $converter_in, '<', $converter_file) {
        my $found = 0;
        while(my $line = <$converter_in>) {
            if($line =~ /^$num\s(.+?)\s*$/) {
                close $converter_in;
                return $1;
            }
        }
        if($found eq 0) {
            return $num;
        }
        close $converter_in;
    }else {
        return $num;
    }
}

# compute hex with object type and object content
sub atm_compute_object_hex {
    my ($object_type, @object_content) = @_;
    my $hasher = "$object_type: ";
    my $content = "";
    foreach my $reading_line(@object_content) {
        $content = $content.$reading_line;
    }
    my $content_length = length $content;
    $hasher = $hasher.$content_length."\0".$content;
    my $object_hex = md5_hex($hasher);
    return $object_hex;
}

# store a file with content
sub atm_store_file {
    my ($file_path, @file_content) = @_;
    if(open my $out, '>', $file_path) {
        foreach my $line(@file_content) {
            print $out $line;
        }
        close $out;
        return 0;
    }else {
        return 1;
    }
}

# append content to a file
sub atm_append_file {
    my ($file_path, @file_content) = @_;
    if(open my $out, '>>', $file_path) {
        foreach my $line(@file_content) {
            print $out $line;
        }
        close $out;
        return 0;
    }else {
        return 1;
    }
}

# read a file
sub atm_withdraw_file {
    my $file_path = $_[0];
    @content = ();
    if(-f $file_path and open my $in, '<', $file_path) {
        while($line = <$in>) {
            push @content, $line;
        }
        close $in;
    }else {
        return 1;
    }
    return (0, @content);
}

# read a sized file
sub atm_withdraw_sized_file {
    my $file_path = $_[0];
    @content = ();
    if(-f $file_path and -s $file_path and open my $in, '<', $file_path) {
        while($line = <$in>) {
            push @content, $line;
        }
        close $in;
    }else {
        return 1;
    }
    return (0, @content);
}

# archive an object
sub util_archive_object {
    my($object_type, @file_content) = @_;
    if(!(-d $legit_git_objects_path)) {
        mkdir($legit_git_objects_path);
    }
    my $object_hex = atm_compute_object_hex($object_type, @file_content);
    if($object_hex =~ /^(.{2})(.{30})$/) {
        my $object_directory = $1;
        my $object_name = $2;
        if(!(-d $legit_git_objects_path.'/'.$object_directory)) {
            mkdir($legit_git_objects_path.'/'.$object_directory);
        }
        my $store_result = atm_store_file($legit_git_objects_path.'/'.$object_directory.'/'.$object_name, @file_content);
        if($store_result eq 0) {
            return (0, $object_hex);
        }else {
            return 1;
        }
    }else {
        return 1;
    }
}

# fetch an object
sub util_fetch_object {
    my $object_hex = $_[0];
    if($object_hex =~ /^(.{2})(.{30})$/) {
        my $object_directory = $1;
        my $object_name = $2;
        my @withdraw_result = atm_withdraw_file($legit_git_objects_path.'/'.$object_directory.'/'.$object_name);
        if($withdraw_result[0] eq 0) {
            return @withdraw_result;
        }else {
            return 1;
        }
    }else {
        return 1;
    }
}

# fetch tree object to a hashed array
sub util_fetch_tree_with_dict {
    my @fetch_tree_object_result = util_fetch_object($_[0]);
    my %tree_blobs = ();
    if($fetch_tree_object_result[0] eq 0) {
        foreach my $blob_line(@fetch_tree_object_result[1 .. $#fetch_tree_object_result]) {
            if($blob_line =~ /^\s*(.+?)\s(.+?)\s*$/) {
                $tree_blobs{$1} = $2;
            }
        }
        return (0, %tree_blobs);
    }else {
        return 1;
    }
}

# save index with a hashed array using file names as keys
sub util_export_index_with_dict {
    my %index_files = @_;
    if(open my $index_out, '>', $legit_git_index) {
        foreach my $file_entry(sort(keys %index_files)) {
            print $index_out $file_entry." ".$index_files{$file_entry}."\n";
        }
        close $index_out;
        return 0;
    }else {
        return 1;
    }
}

# read index to a hashed array using file names as keys
sub util_import_index_with_dict {
    my @index_withdraw_result = atm_withdraw_sized_file($legit_git_index);
    if($index_withdraw_result[0] eq 0) {
        my %index_files = ();
        foreach my $index_line(@index_withdraw_result[1 .. $#index_withdraw_result]) {
            my $index_line_trim = $index_line;
            $index_line_trim =~ s/^\s+|\s+$//g;
            if($index_line_trim =~ /^(.+?)\s(.*)$/) {
                $index_files{$1} = $2;
            }
        }
        return (0, %index_files);
    }else {
        return 1;
    }
}

# read index to an array
sub util_import_index_with_array {
    return atm_withdraw_sized_file($legit_git_index);
}

####################################################################################################
# start of the execution
####################################################################################################

handle_command();

####################################################################################################
# end of the execution
####################################################################################################